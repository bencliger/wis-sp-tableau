const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const s3Promised = (type, params) => new Promise((resolve, reject) => 
  s3[type](params, (err, data) => {
    if (err) {
      return reject(err);
    } else {
      return resolve(data);
    }
  })
);

module.exports = s3Promised;
