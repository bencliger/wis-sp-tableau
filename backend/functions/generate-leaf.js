
const request = require('request').defaults({ encoding: null });
const fetch = require('node-fetch');
const csv = require("csvtojson/v2");
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

const tableauAPIversion = '3.4';
const tableauServer = 'https://analytics.ovumkc.com';
//const tableauAPIversion = '3.8';
//const tableauServer = 'https://stage-tableau.informa.com';
const tableauAPIurl = `${tableauServer}/api/${tableauAPIversion}`;

const uploadFileToS3 = (Body, fileName, bucketName, ContentType) =>
  new Promise((resolve, reject) => {
    s3.putObject({
      Bucket: bucketName,
      Key: fileName,
      Body,
      ContentType
    }, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
});

function isInConcatGeographyFilter(row, concatGeographyFilter, keyOfCountry) {
  if (!concatGeographyFilter) return true;
  const country = row[keyOfCountry][' Country'];
  if (country === 'World') return true;
  return concatGeographyFilter
    .filter(
      g => g.formattedValue
        .replace(`${g.formattedValue.split('-', 2)
        .join('-')}- `, '') === country
    ).length > 0;
}

function isInCompaniesFilter(row, companiesFilter, keyOfCompany) {
  // the country name and company name come back as a 
  // concatenated string from Tableau API so we need
  // to separate them out to get just the company name
  // same applies to geography and countries filter above
  if (!companiesFilter) return true;
  const companyName =
    row[keyOfCompany][' Company'];
  if (companyName === 'All') return true;
  return companiesFilter
      .filter(
        company => company.formattedValue === companyName
      ).length > 0;
}

function isInCountriesFilter(row, countriesFilter, keyOfCountry) {
  if (!countriesFilter) return true;
  const countryName =
  row[keyOfCountry][' Country'];
  
  if (countryName === 'All') return true;
  return countriesFilter
      .filter(
        company => company.formattedValue === countryName
      ).length > 0;
}

function getKeyName(startingString, item) {
  let keyName = '';
  Object.keys(item).forEach(key => {
    if (key.startsWith(startingString)) keyName = key;
  })
  return keyName;
}

function getValuesOfKeysToConcat(item) {
  const lowerCaseRegex = new RegExp(/^[a-z]/);
  return Object.keys(item)
    .filter(key =>
      key.length === 1 && lowerCaseRegex.test(key)
    );
}

function generateUniqueKey(item, columnKeys) {
  let key = '';
  columnKeys.forEach(columnKey => {
    key = `${key} ${item[columnKey][Object.keys(item[columnKey])[0]]}`;
  });
  return key;
}

function getStartAndEndDates(params, isQuarterly) {
  let startYear, endYear;
  params.forEach(param => {
    if (param.name === 'Date_Start Year') startYear = Number(param.value);
    if (param.name === 'Date_End Year') endYear = Number(param.value);
  });
  return {
    startingQuarter: 
      isQuarterly && params
        .filter(param => param.name === 'Date_Start Quarter')[0]
        .value
        .replace('Q',''),
    endingQuarter: 
      isQuarterly && params
        .filter(param => param.name === 'Date_End Quarter')[0]
        .value
        .replace('Q',''),
    startYear,
    endYear
  }
}

function getYearRangeArray(params, firstItem, dateKey) {
  const isQuarterly = firstItem[dateKey].startsWith('Q');
  const { startingQuarter, endingQuarter, startYear, endYear } = getStartAndEndDates(params, isQuarterly);
  const numberOfYears = endYear - startYear;
  const yearsArray = [];

  function generateQuarters(startingIndex, endingIndex, year) {
    for (let i = startingIndex; i <= endingIndex; i++) {
      yearsArray.push(`Q${i} ${year}`);
    }
  }

  if (isQuarterly) {
    switch(numberOfYears) {
      case 0:
        generateQuarters(startingQuarter, endingQuarter, startYear);
        break;
      case 1:
        generateQuarters(startingQuarter, 4, startYear);
        generateQuarters(1, endingQuarter, endYear);
        break;
      default:
        generateQuarters(startingQuarter, 4, startYear);
        for (let j = startYear + 1; j <= endYear - 1; j++) {
          generateQuarters(1, 4, j);
        }
        generateQuarters(1, endingQuarter, endYear);
    }
  } else {
    for (let j = startYear; j <= endYear; j++) {
      yearsArray.push(`${j}`);
    }
  }

  return yearsArray;
}

function generateHeaders(columnKeys, yearRange) {
  return [
    //'uniqueKey', // use to debug row consolidation
    ...columnKeys,
    ...yearRange
  ]
}

const COLORS = {
  Reported: '000000',
  'Market Intelligence': '2266f2',
  Forecast: '106105',
  Estimate: 'c71d14',
};

function findMetric(item, valueKey) {
  if (item[valueKey].includes("%")) return '%';
  if (item[valueKey].length > 0) return '$';
  return null;
}

function getKeyOf(firstItem, columnKeys, propertyName) {
  return columnKeys
    .filter(key => Object.keys(firstItem[key])[0] === propertyName)[0];
}

function callback(error) {
  if (error) fail(error); 
}

let fail;

module.exports = async (event, ctx, cb) => {
  const {
    filtersUri,
    paramsUri,
    companiesFilter,
    countriesFilter,
    concatGeographyFilter,
    params,
    allCompaniesSelected,
    downloadId,
    socketId,
    token,
    viewSlug
  } = event;

  fail = e => {
    console.log('❌ Failed:');
    console.log(e);
    return fetch(`${process.env.TABLEAU_HOOK_URL}/export/tableau-hook`,{
      method: 'POST',
      body: JSON.stringify({
        socketId,
        downloadUrl: null,
        downloadId
      }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(() => cb(Error('Lambda failed')))
      .catch(() => console.log('Hook failed'));
  }

  try {  
    const url = `${tableauAPIurl}${viewSlug}?${filtersUri}${paramsUri}`;
    console.log(url)
    var options = {
      url,
      headers: {
        'X-Tableau-Auth': token
      }
    };
    const jsonObj = await csv({ noheader: false }).fromStream(request(options, callback));
    console.log('Successfully recieved response, processing file...');
    if (jsonObj.length === 0) {
      console.log('--- No rows in response ---')
      return cb(null, {
        statusCode: 200,
        body: {
          noResults: true
        }
      });
    }
    const firstItem = jsonObj[0]; // we use the first item to generate some headers etc
    console.log(firstItem)
    if ((firstItem && firstItem['<html>']) || !firstItem) {
      return fail({
        message: 'Request too large'
      });
    }
    const valueKey = getKeyName('Value_Label', firstItem);
    const dateKey = getKeyName('Date', firstItem);
    const columnKeys = getValuesOfKeysToConcat(firstItem);
    const yearRange = getYearRangeArray(params, firstItem, dateKey);
    const headersArray = generateHeaders(columnKeys, yearRange);
    const keyOfCompany = getKeyOf(firstItem, columnKeys, ' Company');
    const keyOfCountry = getKeyOf(firstItem, columnKeys, ' Country');

    const worksheetCols = headersArray.map(header => ({
      header:
        columnKeys
          .filter(h => h === header).length > 0
            ? `${header}. ${Object.keys(firstItem[header])[0]}`
            : header,
      key: header,
      width: 15
    }));

    const newObject = {};
    const nonEmptyCols = {};
    yearRange.forEach(year => nonEmptyCols[year] = false);

    jsonObj.forEach(item => {
      if (
        (allCompaniesSelected || !keyOfCompany || isInCompaniesFilter(item, companiesFilter, keyOfCompany)) &&
        isInCountriesFilter(item, countriesFilter, keyOfCountry) &&
        isInConcatGeographyFilter(item, concatGeographyFilter, keyOfCountry)
      ) {
        const key = generateUniqueKey(item, columnKeys);
        if (!newObject[key]) {
          const values = {};
          headersArray.forEach(header => {
            values[header] =
              typeof item[header] === 'object'
                ? item[header][Object.keys(item[header])[0]]
                : item[header]
          })
          newObject[key] = {
            ...values
          }
        }
        newObject[key].uniqueKey = key;
        newObject[key][item[dateKey]] = item[valueKey];
        if (!nonEmptyCols[item[dateKey]]) {
          nonEmptyCols[item[dateKey]] = item[valueKey].length > 0;
        }

        newObject[key][`colour_${item[dateKey]}`] = COLORS[item['Colour_Source (In Table)']];
        newObject[key][`unit_${item[dateKey]}`] = findMetric(item, valueKey);
      }
    });
    const fileName = `${new Date().getTime().toString()}.json`;
    const bucketName = process.env.TABLEAU_LEAFS_BUCKET;
    
    const s3Data = JSON.stringify({
      newObject,
      nonEmptyCols,
      yearRange,
      worksheetCols
    });

    await
      uploadFileToS3(s3Data, fileName, bucketName, 'application/json')
        .then(() => cb(null, {
          statusCode: 200,
          body: {
            fileName
          }
        }))
        .catch(e => {
          console.log('❌ uploading to s3:');
          fail(e);
        });
  } catch(e) {
    fail(e);
  }
};