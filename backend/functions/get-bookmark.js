const s3Promised = require('../utils/s3-promises');

module.exports = async (event, ctx, cb) => {
  const { userId, tabName, workbookName } = event.queryStringParameters;
  const Key = `${userId}-${tabName}-${workbookName}`;
  let fileContents, hasExistingBookmark;

  try { 
    await s3Promised('headObject', {
      Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
      Key
    }).then(() => {
      hasExistingBookmark = true;
    });
  } catch (e) {
    if (e.statusCode === 404) {
      hasExistingBookmark = false;
    } else {
      console.log(e);
      return cb(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      });
    }
  }

  if (!hasExistingBookmark) {
    return cb(null, {
      statusCode: 200,
      body: JSON.stringify([]),
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  try { 
    await s3Promised('getObject', {
      Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
      Key
    }).then(resp => {
      fileContents = resp.Body.toString();
    });
  } catch (e) {
    console.log(e);
    return cb(null, {
      statusCode: 500,
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    });
  }
  return cb(null, {
    statusCode: 200,
    body: fileContents,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  });
}