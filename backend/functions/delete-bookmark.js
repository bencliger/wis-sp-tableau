const s3Promised = require('../utils/s3-promises');

module.exports = async (event, ctx, cb) => {
  const { userId, tabName, workbookName, bookmarkId } = JSON.parse(event.body);
  const Key = `${userId}-${tabName}-${workbookName}`;
  let fileContents;

  try { 
    await s3Promised('getObject', {
      Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
      Key
    }).then(resp => {
      fileContents = JSON.parse(resp.Body.toString());
    });
  } catch (e) {
    console.log(e);
    return cb(null, {
      statusCode: 500,
      headers: {
        'Access-Control-Allow-Origin': '*'
      }
    });
  }

  fileContents = JSON.stringify(fileContents.filter(f => f.id !== bookmarkId));

  await s3Promised('putObject', {
    Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
    Key,
    Body: fileContents
  });

  return cb(null, {
    statusCode: 200,
    body: fileContents,
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  });
}