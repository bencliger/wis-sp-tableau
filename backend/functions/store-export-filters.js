const AWS = require('aws-sdk');
const s3 = new AWS.S3();

module.exports = (event, ctx, cb) => {
  const { 
    fileNameWithDate,
    params,
    filters
  } = JSON.parse(event.body);

  s3.putObject({
    Bucket: process.env.ALL_EXPORTS_BUCKET,
    Key: fileNameWithDate,
    Body: JSON.stringify({
      filters,
      params
    })
  }, (err, data) => {
    if (err) {
      return cb(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*'
        },
        body: JSON.stringify({
          err
        })
      });
    } else {
      return cb(null, {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      });
    }
  });
}