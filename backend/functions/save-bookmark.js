const s3Promised = require('../utils/s3-promises');
const sortBookmarksByDate = require('../utils/sort-bookmarks-by-date');

module.exports = async (event, ctx, cb) => {
  const body = JSON.parse(event.body);
  const { userId, tabName, workbookName } = body;
  const Key = `${userId}-${tabName}-${workbookName}`;
  let hasExistingBookmark;
  let bookmarks = [];

  try { 
    await s3Promised('headObject', {
      Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
      Key
    }).then(() => {
      hasExistingBookmark = true;
    });
  } catch (e) {
    if (e.statusCode === 404) {
      hasExistingBookmark = false;
    } else {
      console.log(e);
      return cb(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      });
    }
  }

  console.log(` has existing bookmark is ${hasExistingBookmark}`)

  if (hasExistingBookmark) {
    try { 
      await s3Promised('getObject', {
        Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
        Key,
        ResponseContentType: 'application/json'
      }).then(resp => {
        const existingBookmarks = JSON.parse(resp.Body.toString());
        bookmarks.push(...existingBookmarks);
      });
    } catch (e) {
      console.log(e);
      return cb(null, {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*'
        }
      });
    }
  }

  const dateCreated = new Date();
  bookmarks.push({
    ...body,
    dateCreated: dateCreated.toString(),
    id: dateCreated.getTime().toString(),
    userId
  });

  bookmarks.sort(sortBookmarksByDate);

  await s3Promised('putObject', {
    Bucket: process.env.TABLEAU_BOOKMARKS_BUCKET,
    Key,
    Body: JSON.stringify(bookmarks)
  });

  return cb(null, {
    statusCode: 200,
    body: JSON.stringify(bookmarks),
    headers: {
      'Access-Control-Allow-Origin': '*'
    }
  });
}