const fetch = require('node-fetch');
const Excel = require('exceljs');
const AWS = require('aws-sdk');

const s3 = new AWS.S3();
const lambda = new AWS.Lambda({
  maxRetries: 0,
  httpOptions: {
    timeout: 900000
  }
});

const maxYearsPerLambda = 5;
const tableauAPIversion = '3.4';
const tableauServer = 'https://analytics.ovumkc.com';
//const tableauServer = 'https://stage-tableau.informa.com';
const tableauAPIurl = `${tableauServer}/api/${tableauAPIversion}`;
const tableauSiteId = 'eba6d75d-e662-48fd-8754-b029e72f4cbb';

const tabelauCredentials = {
  credentials: {
    name: process.env.TABLEAU_USER_NAME,
    password: process.env.TABLEAU_USER_PASSWORD,
    site: {
      contentUrl: ""
    }
  }
};

const keyInfo = [
  [],
  [],
  ['Colors', 'Keys'],
  ['Black', 'Collected from publicly available source (example: from an Annual Report)'],
  ['Blue', 'Market intelligence'],
  ['Red', 'Estimated, computed based on Omdia\'s algorithm'],
  ['Green', 'Omdia forecast'],
  [],
  [],
];

const copyrightText = `
  Date: ${new Date().toUTCString()}
  Omdia, Informa Tech
  Copyright Informa ${new Date().getFullYear()}. All rights reserved

  The contents of this product are protected by international copyright laws, database rights and other intellectual property rights.
  The owner of these rights is Informa Telecoms and Media Limited, our affiliates or other third party licensors. All product and
  company names and logos contained within or appearing on this product are the trademarks, service marks or trading names of their
  respective owners, including Informa Telecoms and Media Limited. This product may not be copied, reproduced, distributed or transmitted
  in any form or by any means without the prior permission of Informa Telecoms and Media Limited.

  Whilst reasonable efforts have been made to ensure that the information and content of this product was correct as at the date of first
  publication, neither Informa Telecoms and Media Limited nor any person engaged or employed by Informa Telecoms and Media Limited accepts
  any liability for any errors, omissions or other inaccuracies. Readers should independently verify any facts and figures as no liability
  can be accepted in this regard - readers assume full responsibility and risk accordingly for their use of such information and content.

  Any views and/or opinions expressed in this product by individual authors or contributors are their personal views and/or opinions and
  do not necessarily reflect the views and/or opinions of Informa Telecoms and Media Limited.
`;

function generateYears(startYear, endYear, index) {
  const calculatedStartYear = startYear + (maxYearsPerLambda * index);
  let calculatedEndYear = calculatedStartYear + (maxYearsPerLambda - 1);
  calculatedEndYear = calculatedEndYear <= endYear ? calculatedEndYear : endYear;
  return {
    calculatedStartYear,
    calculatedEndYear
  };
}

function generateYearsUri(calculatedStartYear, calculatedEndYear) {
  return `&vf_${encodeURIComponent('Date_Start Year')}=${calculatedStartYear}&vf_${encodeURIComponent('Date_End Year')}=${calculatedEndYear}`;
}

function generateQuarters(startQuarter, endQuarter) {
  const startQuarterUri = `&vf_${encodeURIComponent('Date_Start Quarter')}=${encodeURIComponent(startQuarter)}`;
  const endQuarterUri = `&vf_${encodeURIComponent('Date_End Quarter')}=${encodeURIComponent(endQuarter)}`;
  return `${startQuarterUri}${endQuarterUri}`;
}

function generateDateUri(dates, numberOfQueries, index) {
  let extraParamsUri = '';
  const startYear = Number(dates['Date_Start Year']);
  const endYear = Number(dates['Date_End Year']);
  const yearsObj = generateYears(startYear, endYear, index);
  const years = generateYearsUri(yearsObj.calculatedStartYear, yearsObj.calculatedEndYear);

  if (numberOfQueries === 1) {
    Object.keys(dates).forEach(k => {
      extraParamsUri = extraParamsUri + `&vf_${encodeURIComponent(k)}=${encodeURIComponent(dates[k])}`;
    })
  } else {
    if (index === 0)
      extraParamsUri = generateQuarters(dates['Date_Start Quarter'], 'Q4');
    
    if (index === numberOfQueries - 1) 
      extraParamsUri = generateQuarters('Q1', dates['Date_End Quarter']);
    
    if (index > 0 && index < numberOfQueries - 1)
      extraParamsUri = generateQuarters('Q1', 'Q4');
    
    extraParamsUri = `${extraParamsUri}${years}`;      
  }
  return extraParamsUri;
}

function isMarketShareSelected(params) {
  const indicatorValue = params.filter(param => param.name === "Indicator");
  return indicatorValue.length > 0 && indicatorValue[0].value === "Market Share";
}

function getTableauToken() {
  return fetch(`${tableauAPIurl}/auth/signin`, { 
    method: 'POST', 
    body: JSON.stringify(tabelauCredentials),
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  })
    .then(resp => resp.json())
    .then(parsedJSON => parsedJSON.credentials.token)
    .catch(e => fail(e));
}

function getTableauView(viewName, workbookName, token, marketShareIsSelected) {
  return fetch(`${tableauAPIurl}/sites/${tableauSiteId}/views?pageSize=1000`, { 
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'X-Tableau-Auth': token
    }
  })
    .then(resp => resp.json())
    .then(parsedJSON => {
      const { view } = parsedJSON.views;
      workbookNameWithMarketshare = marketShareIsSelected
        ? `${workbookName}_market-share`
        : workbookName;
      const filteredView =
        view
          .filter(v => 
            v.name === viewName &&
            v.contentUrl.startsWith(workbookNameWithMarketshare)
          )[0]

      return `/sites/${tableauSiteId}/views/${filteredView.id}/data`;
    })
    .catch(e => fail(e));
}

function hasAllCompaniesSelected(companiesFilter) {
  if (!companiesFilter) return true;
  return companiesFilter.length > 0 && !companiesFilter[0].value
}

function filterOutEmptyColumns(worksheetCols, nonEmptyCols) {
  return worksheetCols.filter(col => {
    if (col.header in nonEmptyCols) {
      return nonEmptyCols[col.header];
    } else {
      return true;
    }
  });
}

function convertObjectToArray(newObject) {
  return Object.keys(newObject)
    .map(key => newObject[key])
    .sort(function(a, b){
      if(a.uniqueKey < b.uniqueKey) return -1;
      if(a.uniqueKey > b.uniqueKey) return 1;
      return 0;
    });
}

const uploadFileToS3 = (buffer, fileName, bucketName) =>
  new Promise((resolve, reject) => {
    s3.putObject({
      Bucket: bucketName,
      Key: fileName,
      Body: buffer
    }, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
});

function reduceFilters(filters) {
  return filters
    .map(filter => [filter.name,
      filter.value.length > 5
        ? '> 5 selected'
        : filter.value
          .map(filterVal => filterVal.formattedValue)
          .join(", ")])
}

let fail;

module.exports = async (event, ctx, cb) => {
  console.log('starting master')
  const parsedBody = JSON.parse(event.body);
  const {
    dates,
    paramsUri,
    companiesFilter,
    params,
    filters,
    viewName,
    workbookName,
    downloadName,
    socketId,
    downloadId
  } = parsedBody;

  const allCompaniesSelected = hasAllCompaniesSelected(companiesFilter);

  fail = e => {
    console.log('❌ Failed:');
    console.log(e);
    return fetch(`${process.env.TABLEAU_HOOK_URL}/export/tableau-hook`,{
      method: 'POST',
      body: JSON.stringify({
        socketId,
        downloadUrl: null,
        downloadId
      }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(() => cb(Error('Lambda failed')))
      .catch(() => console.log('Hook failed'));
  }

  let token, viewSlug;
  const marketShareIsSelected = isMarketShareSelected(params);

  try {
    token = await getTableauToken();
    viewSlug = await getTableauView(viewName, workbookName, token, marketShareIsSelected);
  } catch(e) {
    fail(e);
  }

  const yearRangeForParam = dates['Date_End Year'] - dates['Date_Start Year'] + 1;
  const numberOfQueries = Math.ceil(yearRangeForParam / maxYearsPerLambda);
  const promisesArray = [];
  console.log(`This query has been split into ${numberOfQueries} smaller ${maxYearsPerLambda} year queries`);
  for (let i = 0; i < numberOfQueries; i++) {
    promisesArray.push(new Promise((resolve, reject) => 
      lambda.invoke({
        FunctionName: `tableau-export-rest-${process.env.STAGE}-generateDownloadLinkLeaf`,
        Payload: JSON.stringify({
          ...parsedBody, 
          paramsUri: `${paramsUri}${generateDateUri(dates, numberOfQueries, i)}`,
          token,
          viewSlug,
          downloadId,
          marketShareIsSelected,
          allCompaniesSelected
        })
      }, function(err, data) {
        if (err) {
          console.log('---- lambda came back with error ----')
          console.log(err)
          reject(err);
        }
        else resolve(data);
      })
    ));
  }

  let newObject = {};
  let nonEmptyCols = {};
  let worksheetCols = [];
  let yearRange;
  const workbook = new Excel.Workbook();
  const worksheet = workbook.addWorksheet('informa');

  bucketPromisesArray = [];

  await Promise.all(promisesArray)
    .then(responses => {
      responses.forEach(response => {
        const resp = JSON.parse(response.Payload);
        if (!resp.body.noResults) {
          bucketPromisesArray.push(new Promise((resolve, reject) => {
            const s3Options = {
              Bucket: process.env.TABLEAU_LEAFS_BUCKET,
              Key: resp.body.fileName,
              ResponseContentType: 'application/json'
            };
            s3.getObject(s3Options, function(err, data) {
              if (err) reject(err);
              resolve(JSON.parse(data.Body.toString()));
            });
          }));
        }
      })
    })
    .catch(e => fail(e));

  console.log('Done with the leafs and uploaded to s3')

  await Promise.all(bucketPromisesArray)
    .then(responses => {
      responses.forEach(resp => {
        Object.keys(resp.newObject).forEach(key => {
          if (!newObject[key]) {
            newObject[key] = {...resp.newObject[key]}
          } else {
            newObject[key] = {...newObject[key], ...resp.newObject[key]}
          }
        });
        Object.keys(resp.nonEmptyCols).forEach(key => {
          if (!nonEmptyCols[key]) {
            nonEmptyCols[key] = resp.nonEmptyCols[key];
          } else {
            nonEmptyCols[key] = {...nonEmptyCols[key], ...resp.nonEmptyCols[key]}
          }
        });
        if (!yearRange) yearRange = resp.yearRange;
        if (worksheetCols.length === 0) worksheetCols.push(...resp.worksheetCols)
      })
    })
    .catch(e => fail(e));

  console.log('Done retrieving the leafs from s3')

  worksheet.columns = filterOutEmptyColumns(worksheetCols, nonEmptyCols);

  const newArray = convertObjectToArray(newObject);
  newArray.forEach(item => {
    const newRow = worksheet.addRow(item);
    yearRange.forEach(year => {
      if (year in nonEmptyCols && !nonEmptyCols[year]) return;
      if (item[`colour_${year}`]) {
        const cell = worksheet.getCell(newRow.getCell(year)._address);
        cell.style = {
          font: {
            color: {
              argb: item[`colour_${year}`]
            }
          }
        };
        const parsedValue = parseFloat(cell.value.replace(/,/g, ''));
        if (cell.value) {
          if (item[`unit_${year}`] === '%') {
            cell.numFmt = "0.00%";
            cell.value = parsedValue / 100;
          } else {
            cell.numFmt = parsedValue % 1 != 0 ? "#,##0.00" : "#,##";
            cell.value = parsedValue;
          }
        }
      }
    })
  });

  // Add info about the colour keys
  worksheet.addRows(keyInfo);
  // add filter and param info...
  const paramMap = params.map(param => [param.name, param.value]);
  const filterMap = reduceFilters(filters);
  worksheet.addRows([[], ...filterMap, []]);
  worksheet.addRows([[], ...paramMap, []]);
  // add copyright text
  worksheet.addRows(copyrightText.split('\n').map(line => [line]));
  const buffer = await workbook.xlsx
    .writeBuffer()
    .catch(e => {
      console.log('❌ error writing buffer:');
      throw new Error(e);
    });

  const bucketName = process.env.DOWNLOAD_BUCKET;
  const bucket = `https://${bucketName}.s3-eu-west-1.amazonaws.com`;
  const downloadUrl = `${bucket}/${downloadName}`;

  await
    uploadFileToS3(buffer, downloadName, bucketName)
      .then(() => {
        console.log('done uploading final file to s3')
        fetch(`${process.env.TABLEAU_HOOK_URL}/export/tableau-hook`,{
          method: 'POST',
          body: JSON.stringify({
            socketId,
            downloadUrl,
            downloadId
          }),
          headers: { 'Content-Type': 'application/json' }
        })
          .catch(() => console.log('Hook failed'))
        })
        .catch(e => {
          console.log('❌ uploading to s3:');
          fail(e);
        }); 

  return cb(null, {
    statusCode: 202
  });
}
