import request from 'superagent';

export default () => new Promise((resolve, reject) => {
  try {
    const workbook = window.vis.getWorkbook();
    const workbookName = workbook.getName();
    const activeSheet = workbook.getActiveSheet();
    const tabName = activeSheet.getName();
    const userId = window.OVUMANALYTICS.samsUserId;
    request
      .get(`${window.tableauConfig.tableauRestEndpoint}/get-bookmark?userId=${userId}&tabName=${tabName}&workbookName=${workbookName}`)
      .then(resp => {
        resolve(resp.body)
      })
      .catch(e => { throw new Error(e) })
    } catch(e) {
      reject(e);
    }
});