import sendFilterAnalytics from '../lib/send-filter-anaytics';

export default (clientId, downloadName) => new Promise((resolve, reject) => {
  try {
    const workbook = window.vis.getWorkbook();
    const activeSheet = workbook.getActiveSheet();
    const worksheets = activeSheet.getWorksheets();
    let tableSheet = null;

    const viewName = activeSheet.getName();
    const workbookName = workbook.getName();
    
    const transformedFilters = [];
    let parameters;
    let paramsUri = '';
    let filtersUri = '';
    let dates = {};
    let companiesFilter,
      countriesFilter,
      concatGeographyFilter,
      hasMarketShareSelected;

    workbook
      .getParametersAsync()
      .then(params => {
        parameters = params.map(param => {
          return {
            name: param.getName(),
            value: param.getCurrentValue().formattedValue
          }
        });
        params.forEach(p => {
          const value = p.getCurrentValue().formattedValue;
          const name = p.getName();
          if (name === "Indicator" && value === "Market Share") {
            hasMarketShareSelected = true;
          }

          if (
            name === "Date_Start Quarter" ||
            name === "Date_End Quarter" ||
            name === "Date_Start Year" ||
            name === "Date_End Year"
          ) {
            dates[name] = value;
          } else {
            const formattedName = `${encodeURIComponent(name)}`;
            const formattedValue = `${encodeURIComponent(value)}`;
            const joinedValue = `&vf_${formattedName}=${formattedValue}`;
            paramsUri = paramsUri + joinedValue;
          }
        });
        worksheets.forEach(worksheet => {
          const sheetName = worksheet.getName();
          if (sheetName.indexOf('_Table') > -1) {
            if (hasMarketShareSelected) {
              if (sheetName.indexOf('Market Share') > -1) {
                tableSheet = worksheet;
              }
            } else {
              if (sheetName.indexOf('Market Share') === -1) {
                tableSheet = worksheet;
              }
            }
          }
        });
        return true;
      })
        .then(() => tableSheet.getFiltersAsync())
        .then(filters => {
          const transformedFiltersAnalytics = [];
          filters.forEach((filter) => {
            const filterName = filter.getFieldName();
            const appliedValues = filter.getAppliedValues();
            switch (filterName) {
              case 'Company':
                companiesFilter = appliedValues;
                break;
              case 'Country':
                countriesFilter = appliedValues;
                break;
              case 'Concat_Geography':
                concatGeographyFilter = appliedValues;
                break;
              case 'Filter_Indicator':
                transformedFilters.push({
                  name: filter.getFieldName(),
                  value: hasMarketShareSelected 
                    ? [{ formattedValue: "Market Share", value: "Market Share" }]
                    : appliedValues
                })
                break;
              default: 
                transformedFilters.push({
                  name: filter.getFieldName(),
                  value: appliedValues
                });
                break;
            }

            transformedFiltersAnalytics.push({
              name: filterName,
              value:
                filterName === 'Company' && !appliedValues[0].value
                  ? 'All'
                  : appliedValues
            });
          });
          
          transformedFilters.forEach(filter => {
            const name = encodeURIComponent(filter.name);
            let value = '';
            filter.value.forEach(v => {
              if (v.value) {
                value = `${value.length > 0 ? value + '%2C' : value}${encodeURIComponent(v.value)}`;
              }
            });

            if (value.length > 0) {
              const consolidatedValue = `vf_${name}=${value}`;
              filtersUri = `${filtersUri.length > 0 ? filtersUri + '&' : filtersUri}${consolidatedValue}`;
            } else {
              console.log(`${name} does not have a value`);
            }
          });

          sendFilterAnalytics(transformedFiltersAnalytics, parameters);
          const downloadId = new Date().getTime();

          resolve({
            'client_id': clientId,
            filters: transformedFilters,
            params: parameters,
            filtersUri,
            paramsUri,
            downloadId,
            companiesFilter,
            countriesFilter,
            concatGeographyFilter,
            viewName,
            workbookName,
            dates,
            downloadName: `${downloadName}.xlsx`
          });
        });
    } catch(e) {
      reject(e);
    }
});