export default () => new Promise((resolve, reject) => {
  try {
    const workbook = window.vis.getWorkbook();
    const workbookName = workbook.getName();
    const activeSheet = workbook.getActiveSheet();
    const worksheets = activeSheet.getWorksheets();
    const tabName = activeSheet.getName();
    let tableSheet, hasMarketShareSelected, parameters, filters;

    workbook
      .getParametersAsync()
      .then(params => {
        parameters = params.map(param => ({
          name: param.getName(),
          value: param.getCurrentValue().formattedValue
        }));
        params.forEach(p => {
          const value = p.getCurrentValue().formattedValue;
          const name = p.getName();
          if (name === "Indicator" && value === "Market Share") {
            hasMarketShareSelected = true;
          }
        });

        worksheets.forEach(worksheet => {
          const sheetName = worksheet.getName();
          if (sheetName.indexOf('_Table') > -1) {
            if (hasMarketShareSelected) {
              if (sheetName.indexOf('Market Share') > -1) {
                tableSheet = worksheet;
              }
            } else {
              if (sheetName.indexOf('Market Share') === -1) {
                tableSheet = worksheet;
              }
            }
          }
        });
        return true;
      })
      .then(() => tableSheet.getFiltersAsync())
      .then(f => {
        filters = f.map(fil => ({
          name: fil.getFieldName(),
          value:
            fil.getIsAllSelected() 
              ? 'All' 
              : fil.getAppliedValues()
        }));

        const bookmarkObject = {
          filters,
          parameters,
          tabName,
          workbookName,
          userId: window.OVUMANALYTICS.samsUserId
        }

        resolve(bookmarkObject);
      })
    } catch(e) {
      reject(e);
    }
});