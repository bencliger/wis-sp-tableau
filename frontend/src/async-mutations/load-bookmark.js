export default bookmark => new Promise((resolve, reject) => {
  const { filters, parameters } = bookmark;
  const workbook = window.vis.getWorkbook();
  const sheets = workbook.getActiveSheet().getWorksheets();
  const sheetName = '_Table';
  const hasMarketShareSelected = checkIfMarketShareSelected(parameters);
  const sheet = findSheet(sheets, sheetName, hasMarketShareSelected);
  const promises = [];

  for (let i = 0; i < parameters.length; i++) {
    promises.push(new Promise((resolve) => {
      const parameterItem = parameters[i];
      workbook
        .changeParameterValueAsync(parameterItem.name, parameterItem.value)
        .then(resolve)
    }));
  }

  for (let i = 0; i < filters.length; i++) {
    const { name, value } = filters[i];
    if (name !== "Filter_Indicator") {
      promises.push(new Promise((resolve) => {
        const hasAllSelected = value === 'All';
        const changeType = hasAllSelected
          ? window.tableau.FilterUpdateType.ALL
          : window.tableau.FilterUpdateType.REPLACE;
        const mappedValue = hasAllSelected
          ? ''
          : value.map(v => v.formattedValue);

        sheet
          .applyFilterAsync(name, mappedValue, changeType)
          .then(resolve)
      }
    ))}
  }

  Promise.all(promises)
    .then(resolve)
    .catch(reject);
});

function findSheet(sheets, sheetName, hasMarketShareSelected) {
  return sheets.filter(s => 
    s
      .getName()
      .includes(sheetName) && (
        hasMarketShareSelected 
          ? s.getName().includes('Market Share') 
          : !s.getName().includes('Market Share')
        )
  )[0];
}

function checkIfMarketShareSelected(params) {
  let hasMarketShareSelected;
  params.forEach(p => {
    const { name, value } = p;
    if (name === "Indicator" && value === "Market Share") {
      hasMarketShareSelected = true;
    }
  });
  return hasMarketShareSelected;
}
