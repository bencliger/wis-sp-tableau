export default {
  setBookmarks(state, bookmarks) {
    state.bookmarks = bookmarks;
  },
  setBookmarksOpen(state, trueFalse) {
    state.bookmarksOpen = trueFalse;
  },
  updateDownloadCounter(state, value) {
    state.downloadCounter = value;
  },
  setBookmarkBeingDeleted(state, id) {
    state.bookmarkBeingDeletedId = id;
  },
  setCreateNewBookmarkLoading(state, trueFalse) {
    state.createNewBookmarkLoading = trueFalse;
  },
  setBookmarkBeingLoaded(state, id) {
    state.bookmarkBeingLoadedId = id;
  },
  setBookmarksLoading(state, trueFalse) {
    state.bookmarksLoading = trueFalse;
  },
  setTabName(state, tabName) {
    state.tabName = tabName;
  },
  setSocketId(state, socketId) {
    state.socketId = socketId;
  },
  toggleExportDataLoading(state) {
    state.exportDataLoading = !state.exportDataLoading;
  },
  addDownloadToQueue(state, { downloadId, downloadName }) {
    state.downloads = [...state.downloads, {
      id: downloadId,
      downloadFailed: false,
      label: `Exporting report: ${downloadName}`
    }];
  },
  setAllDownloadsFailed(state) {
    const copiedDownloadList = [...state.downloads];
    copiedDownloadList.forEach(download => {
      download.downloadFailed = true;
    });
    state.downloads = copiedDownloadList;
  },
  removeDownloadFromQueue(state, downloadId) {
    state.downloads = [...state.downloads.filter(d => d.id !== downloadId)];
  },
  setDownloadFailedInQueue(state, downloadId) {
    const copiedDownloadList = [...state.downloads];
    copiedDownloadList.forEach(download => {
      if (download.id === downloadId) {
        download.downloadFailed = true;
      }
    });
    state.downloads = copiedDownloadList;
  }
};
