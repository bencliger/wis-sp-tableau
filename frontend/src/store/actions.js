import request from 'superagent';
import generateDownloadPayload from '../async-mutations/generate-download-payload';
import generateBookmarkPayload from '../async-mutations/generate-bookmark-payload';
import fetchBookmarks from '../async-mutations/fetch-bookmarks';
import loadBookmark from '../async-mutations/load-bookmark';

export default {
  loadBookmark ({ commit }, bookmark) {
    commit('setBookmarkBeingLoaded', bookmark.id);
    loadBookmark(bookmark)
      .then(() => {
        commit('setBookmarkBeingLoaded', '');
        commit('setBookmarksOpen', false);
      })
      .catch(e => {
        commit('setBookmarkBeingLoaded', '');
        handleAsyncError(e);
      });
  },
  updateDownloadCounter ({ commit }, value) {
    commit('updateDownloadCounter', value);
  },
  setBookmarksOpen ({ commit }, trueFalse) {
    commit('setBookmarksOpen', trueFalse);
  },
  setAllDownloadsFailed ({ commit }) {
    commit('setAllDownloadsFailed');
  },
  setTabName ({ commit }, tabName) {
    commit('setTabName', tabName);
  },
  setSocketId ({ commit }, socketId) {
    commit('setSocketId', socketId);
  },
  removeDownloadFromQueue ({ commit }, downloadId) {
    commit('removeDownloadFromQueue', downloadId);
  },
  setDownloadFailedInQueue ({ commit }, downloadId) {
    commit('setDownloadFailedInQueue', downloadId);
  },
  fetchBookmarks ({ commit }) {
    commit('setBookmarksLoading', true);
    fetchBookmarks()
      .then(bookmarks => {
        commit('setBookmarks', bookmarks);
        commit('setBookmarksLoading', false);
      })
      .catch(e => {
        commit('setBookmarksLoading', false);
        handleAsyncError(e);
      });
  },
  saveBookmark ({ commit }, bookmarkName) {
    commit('setCreateNewBookmarkLoading', true);
    generateBookmarkPayload()
      .then(payload => {
        request
          .post(`${window.tableauConfig.tableauRestEndpoint}/save-bookmark`)
          .set('Content-Type', 'application/json')
          .send({ ...payload, bookmarkName })
          .then(resp => {
            commit('setBookmarks', resp.body);
            commit('setCreateNewBookmarkLoading', false);
          })
          .catch(e => { throw new Error(e) })
      })
      .catch(e => {
        commit('setCreateNewBookmarkLoading', false);
        handleAsyncError(e);
      });
  },
  deleteBookmark ({ commit }, bookmarkId) {
    commit('setBookmarkBeingDeleted', bookmarkId);
    const workbook = window.vis.getWorkbook();
    const workbookName = workbook.getName();
    const activeSheet = workbook.getActiveSheet();
    const tabName = activeSheet.getName();

    const payload = {
      userId: window.OVUMANALYTICS.samsUserId,
      tabName,
      workbookName,
      bookmarkId
    }

    request
      .delete(`${window.tableauConfig.tableauRestEndpoint}/delete-bookmark`)
      .set('Content-Type', 'application/json')
      .send(payload)
      .then(resp => {
        commit('setBookmarkBeingDeleted', '');
        commit('setBookmarks', resp.body);
      })
      .catch(e => {
        commit('setBookmarkBeingDeleted', '');
        handleAsyncError(e);
      });
  },
  downloadData ({ commit }, { clientId, downloadName }) {
    commit('toggleExportDataLoading');
    generateDownloadPayload(clientId, downloadName)
      .then(payload => {
        request
          .post(`${window.tableauConfig.socketEndpoint}/export/tableau`)
          .set('Content-Type', 'application/json')
          .send(payload)
          .then(() => {
            const { downloadId } = payload;
            commit('addDownloadToQueue', { downloadId, downloadName });
            commit('toggleExportDataLoading');
          })
          .catch(e => { throw new Error(e) })
      })
      .catch(e => {
        commit('toggleExportDataLoading');
        handleAsyncError(e);
      })
  }
}

function handleAsyncError(e) {
  console.log(e);
  alert('There was an error processing your request. Please try again or get in touch with support.')
}