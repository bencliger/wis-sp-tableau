export default {
  bookmarks: [],
  bookmarksLoading: true,
  bookmarksOpen: false,
  bookmarkBeingDeletedId: '',
  bookmarkBeingLoadedId: '',
  createNewBookmarkLoading: false,
  exportDataLoading: false,
  fallbackFileUploadVisible: false,
  tabName: '',
  socketId: null,
  downloadCounter: 0,
  downloads: []
};
