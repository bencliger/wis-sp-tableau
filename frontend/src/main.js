import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
import 'es6-promise/auto';

import storeObject from './store/store';

Vue.use(Vuex);

Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      if (!(el == event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});

const store = new Vuex.Store(storeObject);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  store
}).$mount('#tableau-app')
