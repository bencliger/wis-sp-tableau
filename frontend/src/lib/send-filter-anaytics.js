function sendFilterAnalytics(filters, params) {
  var xhr = new XMLHttpRequest();
  var currentTime = new Date().getTime();
  var loggedInUserId = window.OVUMANALYTICS ? window.OVUMANALYTICS.samsUserId : 'dev';
  var fileNameWithDate = loggedInUserId + '-' + currentTime + '.json';

  xhr.open("POST", window.tableauConfig.tableauRestEndpoint + '/store-export-filters', true);

  xhr.send(
    JSON.stringify({
      filters,
      params,
      fileNameWithDate
    })
  );
}

export default sendFilterAnalytics;