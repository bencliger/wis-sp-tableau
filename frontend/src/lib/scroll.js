function scrollTo(element, offsetY) {
  if (!offsetY) {
    offsetY = 40;
  }
  var element = document.querySelector(element);
  var y = element.getBoundingClientRect().top + window.scrollY - offsetY;
  window.scroll({
    top: y,
    behavior: 'smooth'
  });
}

module.exports = {
  scrollTo
}