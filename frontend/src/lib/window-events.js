import { setCookie, getCookie } from './cookie';

export default () => {
  window.vis.addEventListener(window.tableau.TableauEventName.FILTER_CHANGE, function (filter) {
    const name = filter.getFieldName();
    if (name === 'Company') {
      window.vis
        .getWorkbook()
        .getParametersAsync()
        .then(params => {
          const cookie = getCookie('marketShareWarnMsg');
          if (cookie) return;
          params.forEach(p => {
            const value = p.getCurrentValue().formattedValue;
            const paramName = p.getName();
            if (paramName === 'Indicator' && value === 'Market Share') {
              setCookie('marketShareWarnMsg', true, 365);
              alert('Please note that filtering on company for market share may affect the presented results');
            }
          });
        })
    }
  });

  window.vis.addEventListener(window.tableau.TableauEventName.PARAMETER_VALUE_CHANGE, function (param) {
    const name = param.getParameterName();
    if (name === 'Indicator') {
      param.getParameterAsync().then(fullParam => {
        const value = fullParam.getCurrentValue();
        if (value.formattedValue === 'Market Share') {
          const workbook = window.vis.getWorkbook();
          const activeSheet = workbook.getActiveSheet();
          const worksheets = activeSheet.getWorksheets();
          let tableSheet = null;

          worksheets.forEach(worksheet => {
            const sheetName = worksheet.getName();
            if (sheetName.indexOf('_Table') > -1 && sheetName.indexOf('Market Share') > -1) {
              tableSheet = worksheet;
            }
          });

          tableSheet.getFiltersAsync()
            .then(filters => {
              const cookie = getCookie('marketShareWarnMsg');
              if (cookie) return;
              filters.forEach((filter) => {
                const filterName = filter.getFieldName();
                if (filterName === 'Company') {
                  if (!filter.getIsAllSelected()) {
                    setCookie('marketShareWarnMsg', true, 365);
                    alert('Please note that filtering on company for market share may affect the presented results');
                  }
                }
              })
            })
        }
      })
    }
  });
}